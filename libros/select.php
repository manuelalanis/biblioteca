<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Busqueda</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/select.css">
	<style>
	.cantidades_fondo{
		background-color: #dfdfdf;
	}
	.cantidades_color{
		background: #ff4d4d;
	}
	</style>
</head>
</html>
<?php error_reporting(0);
	$busqueda_titulo=$_POST['busqueda_titulo'];
	$busqueda_autor=$_POST['busqueda_autor'];
	$busqueda_edicon=$_POST['busqueda_edicon'];
	$busqueda_editorial=$_POST['busqueda_editorial'];

	$busqueda_cotizada=$_POST['busqueda_cotizada'];
	$busqueda_adquirida=$_POST['busqueda_adquirida'];
	$busqueda_recibida=$_POST['busqueda_recibida'];
	$busqueda_existente=$_POST['busqueda_existente'];

	$faltantes=$_POST['caja_faltantes'];

	$select_cotizada=$_POST['select_cotizada'];
	$select_adquirida=$_POST['select_adquirida'];
	$select_recibida=$_POST['select_recibida'];
	$select_existente=$_POST['select_existente'];

	echo "Cotizada: ".$select_cotizada;
	echo "Adquirida: ".$select_adquirida;
	echo "Recibida: ".$select_recibida;
	echo "Existente: ".$select_existente;
	$name = $_POST['opcion'];
	if (!$name) {
		echo "<p class=\"lead\">No selecciono ninguna opcion</p>";
	}
	$text_busqueda="Buscando por ";
	if ($busqueda_titulo) {
		$text_busqueda=$text_busqueda."Titulo: ".'\''.strtoupper($busqueda_titulo).'\'';
	}
	if ($busqueda_autor) {
		$text_busqueda=$text_busqueda." Autor: ".'\''.strtoupper($busqueda_autor).'\'';	
	}
	if ($busqueda_edicon) {
		$text_busqueda=$text_busqueda." Edicion: ".'\''.strtoupper($busqueda_edicon).'\'';	
	}
	if ($busqueda_editorial) {
		$text_busqueda=$text_busqueda." Editorial: ".'\''.strtoupper($busqueda_editorial).'\'';	
	}
	echo "<p class=\"lead\">$text_busqueda</p>";
	if ($busqueda_titulo!=NULL||$busqueda_autor!=NULL||$busqueda_edicon!=NULL) {
		$query="SELECT * FROM `libros` LEFT JOIN `compras` on libros.compras_no_id=compras.no_id INNER JOIN `materia` on libros.materia_id_materia=materia.id_materia INNER JOIN `provedor` on libros.provedor_id_provedor=provedor.id_provedor WHERE libros.titulo like '%$busqueda_titulo%'and libros.autor like '%$busqueda_autor%' and libros.edicion like '%$busqueda_edicon%' and provedor.editorial_provedor like '%$busqueda_editorial%' and libros.c_cotizada like '%$busqueda_cotizada%' and libros.c_recibida like '%$busqueda_recibida%' and libros.c_adquirida like '%$busqueda_adquirida%' and libros.c_existente like '%$busqueda_existente%';";
	}else{
		$query="SELECT * FROM `libros` LEFT JOIN `compras` on libros.compras_no_id=compras.no_id INNER JOIN `materia` on libros.materia_id_materia=materia.id_materia INNER JOIN `provedor` on libros.provedor_id_provedor=provedor.id_provedor WHERE libros.titulo like '%$busqueda_titulo%'and libros.autor like '%$busqueda_autor%' and libros.edicion like '%$busqueda_edicon%' and provedor.editorial_provedor like '%$busqueda_editorial%' and libros.c_cotizada like '%$busqueda_cotizada%' and libros.c_recibida like '%$busqueda_recibida%' and libros.c_adquirida like '%$busqueda_adquirida%' and libros.c_existente like '%$busqueda_existente%';";
	}
	require("../conectar.php");
				?>
				<script type="text/JavaScript">
				$(document).ready(function () {

    function exportTableToCSV($table, filename) {
        var $rows = $table.find('tr:has(td)'),
            tmpColDelim = String.fromCharCode(11), 
            tmpRowDelim = String.fromCharCode(0), 
            colDelim = '","',
            rowDelim = '"\r\n"',
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td.download');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace('"', '""'); 

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }
    $(".export").on('click', function (event) {
        exportTableToCSV.apply(this, [$('#tabla_libros'), 'busqueda_libros.csv']);

    });
});</script>
	<p class="lead"><a href="#" class="export">Exportar a Excel</a></p>
				<table id="tabla_libros"border="0" class="tabla" >
				<tr class="titulos" >
					<td class="oculto head uno" width="190";>Titulo</td>
					<td class="oculto head dos" width="190";>Autor</td>
					<td class="oculto head inclu" id="special"width="190";>Editorial</td>
					<td class="oculto head tres" id="special"width="190";>Edicion</td>
					<td class="oculto head cuatro" width="190";>Año</td>
					<td class="oculto head cinco" width="190";>Isbn</td>
					<td class="oculto head seis" width="190";>Folio</td>
					<td class="oculto head siete cantidades_color" >Cotizada</td>
					<td class="oculto head ocho cantidades_color" >Adquirida</td>
					<td class="oculto head nueve cantidades_color" >Recibida</td>
					<td class="oculto head diez cantidades_color" >Existente</td>
					<td class="oculto head once" width="190";>Materia</td>
					<td class="oculto head doce" width="190";>Provedor</td>
					<td class="oculto head trece" width="190";>Factura</td>
					</tr>
					<?php 
					require("../conectar.php");
					$doQuery = mysql_query($query,$conectado);
					if (!@mysql_fetch_array($doQuery,MYSQL_ASSOC)) {
						?> <p class="lead">No se encontro la busqueda</p> <?php  	
					}else{
					$doQuery = mysql_query($query,$conectado);
					while($fila = @mysql_fetch_array($doQuery,MYSQL_ASSOC))
					{
						$vector[] = $fila;
					}
					foreach($vector as $fila)
					{
					if ($faltantes=="0") 
					{
						?>
						<tr align="center">
							<td class="oculto uno"><?php echo $fila['titulo'];?></td>
							<td class="oculto dos"><?php echo $fila['autor'];?></td>
							<td class="oculto inclu"><?php echo $fila['editorial_libros'];?></td>
							<td class="oculto tres"><?php echo $fila['edicion'];?></td>
							<td class="oculto cuatro"><?php echo $fila['year'];?></td>
							<td class="oculto cinco"><?php echo $fila['isbn'];?></td>
							<td class="oculto seis"><?php echo $fila['folio'];?></td>
							
							<td id="c_cotizada"class="oculto siete cantidades_fondo"><?php echo $fila['c_cotizada'];?></td>
							<td id="c_adquirida"class="oculto ocho cantidades_fondo"><?php echo $fila['c_adquirida'];?></td>
							<td id="c_recibida"class="oculto nueve cantidades_fondo"><?php echo $fila['c_recibida'];?></td>
							<td id="c_existente"class="oculto diez cantidades_fondo"><?php echo $fila['c_existente'];?></td>
							
							<td class="oculto once"><?php echo $fila['materia'];?></td>
							<td class="oculto doce"><?php echo $fila['nombre_provedor'];?></td>
							<td class="oculto trece"><?php echo $fila['factura_nombre']."-".$fila['factura_numero'];?></td>
						</tr>
					<?php	
					}else{//recibidas<adquiridas
						$c_recibida=(int)$fila['c_recibida'];
						$c_adquirida=(int)$fila['c_adquirida'];
						if ($c_recibida<$c_adquirida) {
								?>
						<tr align="center">
							<td class="oculto uno"><?php echo $fila['titulo'];?></td>
							<td class="oculto dos"><?php echo $fila['autor'];?></td>
							<td class="oculto inclu"><?php echo $fila['editorial_libros'];?></td>
							<td class="oculto tres"><?php echo $fila['edicion'];?></td>
							<td class="oculto cuatro"><?php echo $fila['year'];?></td>
							<td class="oculto cinco"><?php echo $fila['isbn'];?></td>
							<td class="oculto seis"><?php echo $fila['folio'];?></td>

							<td id="c_cotizada"class="oculto siete cantidades_fondo"><?php echo $fila['c_cotizada'];?></td>
							<td id="c_adquirida"class="oculto ocho cantidades_fondo"><?php echo $fila['c_adquirida'];?></td>
							<td id="c_recibida"class="oculto nueve cantidades_fondo"><?php echo $fila['c_recibida'];?></td>
							<td id="c_existente"class="oculto diez cantidades_fondo"><?php echo $fila['c_existente'];?></td>
							
							<td class="oculto once"><?php echo $fila['materia'];?></td>
							<td class="oculto doce"><?php echo $fila['nombre_provedor'];?></td>
							<td class="oculto trece"><?php echo $fila['factura_nombre']."-".$fila['factura_numero'];?></td>
						</tr>
					<?php	
						}
					}	
					}
					}
				?>
			</table>
			<?php 
	foreach ($name as $opcion){
    ?>
    <script>
    var vari='<?php echo $opcion  ?>';
    if (vari=='titulo') {
        $('.uno').removeClass('oculto');
        $('.uno').addClass('download');
    };
    if (vari=='autor') {
        $('.dos').removeClass('oculto');
        $('.dos').addClass('download');
    };
    if (vari=='editorial') {
        $('.inclu').removeClass('oculto');
        $('.inclu').addClass('download');
    };
    if (vari=='edicion') {
        $('.tres').removeClass('oculto');
        $('.tres').addClass('download');
    };
    if (vari=='year') {
        $('.cuatro').removeClass('oculto');
        $('.cuatro').addClass('download');
    };
    if (vari=='isbn') {
        $('.cinco').removeClass('oculto');
        $('.cinco').addClass('download');
    };
    if (vari=='folio') {
        $('.seis').removeClass('oculto');
        $('.seis').addClass('download');
    };
    if (vari=='c_cotizada') {
        $('.siete').removeClass('oculto');
        $('.siete').addClass('download');
    };
    if (vari=='c_adquirida') {
        $('.ocho').removeClass('oculto');
        $('.ocho').addClass('download');
    };
    if (vari=='c_recibida') {
        $('.nueve').removeClass('oculto');
        $('.nueve').addClass('download');
    };
    if (vari=='c_existente') {
        $('.diez').removeClass('oculto');
        $('.diez').addClass('download');
    };
    if (vari=='materia') {
        $('.once').removeClass('oculto');
        $('.once').addClass('download');
    };
    if (vari=='provedor') {
        $('.doce').removeClass('oculto');
        $('.doce').addClass('download');
    };
    if (vari=='factura') {
        $('.trece').removeClass('oculto');
        $('.trece').addClass('download');
    };
    </script>
    <?php 
    }
	 ?>
	<?php
 	?>