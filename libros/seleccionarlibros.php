<!DOCTYPE html>
<html lang="en">
<?php
error_reporting(0);
session_start();
if($_SESSION['logged']=='yes')
{
?>
<head>
  <title>Libros</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="libros.css">
  <script>
  $(document).ready(function(){
    $("#busqueda_titulo").focus();
  });
  $(function() {
    var autocompletar_libros = new Array();
    <?php 
     require('busquedas/comp_libros.php');
     for($p = 0;$p < 15; $p++){  
      ?>
       autocompletar_libros.push('<?php echo $arreglo_php[$p]; ?>');
     <?php
      } 
      ?>
    $( "#busqueda_titulo" ).autocomplete({
      source: autocompletar_libros,
    });

    var autocompletar_autor  = new Array();
    <?php 
     require('busquedas/comp_autor.php');
     for($i = 0;$i < 15; $i++){  
      ?>
       autocompletar_autor.push('<?php echo $arreglo_autor[$i]; ?>');
     <?php
      } 
      ?>
    $( "#busqueda_autor" ).autocomplete({
      source: autocompletar_autor,
    });

    var autocompletar_edicion  = new Array();
    <?php 
     require('busquedas/comp_edicion.php');
     for($j = 0;$j < 15; $j++){  
      ?>
       autocompletar_edicion.push('<?php echo $arreglo_edicion[$j]; ?>');
     <?php
      } 
      ?>
    $( "#busqueda_edicon" ).autocomplete({
      source: autocompletar_edicion,
    });

    var autocompletar_editorial  = new Array();
    <?php 
     require('busquedas/comp_editorial.php');
     for($a = 0;$a < 15; $a++){  
      ?>
       autocompletar_editorial.push('<?php echo $arreglo_editorial[$a]; ?>');
     <?php
      } 
      ?>
    $( "#busqueda_editorial" ).autocomplete({
      source: autocompletar_editorial,
    });

  });
  </script>
  <style>
   .cerrar{
    color: white;
    position: relative;
    left:20em;
  }
  input{
    text-transform: uppercase;
  }
  #iframebusqueda{
    position:absolute;
    background-color: white;
  }
  .opciones{
    margin: 5px;
    display: inline-block;
    font-size: 17px;
  }
  .opciones input[type='checkbox']{
    background-color: red;
    width: 15px;
    height: 15px;
  }
  .oculto{
    display: none;
  }
  </style>
  </head>
<body>
  <iframe class="oculto"id="iframelibro"name="iframelibro" src="" frameborder="0">
  </iframe>
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand active" href="../libros.php">Biblioteca UABC</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../libros.php">Libros</a></li>
        <li><a href="../provedores.php">Provedores</a></li>
        <li><a href="../materias.php">Materias</a></li>
        <li><a href="../facturas.php">Facturas</a></li>
        <li class="cerrar"><a href="#"> <?php echo "Usuario: ".$_SESSION['usuario']; ?></a></li>
        <li class="cerrar"><a href="cerrarsesion.php">Cerrar sesion</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="container">
  <h3>Libros</h3>
  <ul class="nav nav-pills">
    <li><a href="../libros.php" >Agregar</a></li>
    <li><a href="buscar2.php">Buscar y exportar</a></li>
    <li><a href="modificar2.php" >Modificar</a></li>
    <li><a href="seleccionarlibros.php">Eliminar</a></li>
  </ul>
</div>

  <p class="lead">Buscar por:</p>
<form action="motoreliminar.php" method="post" target="iframebusqueda">
     <div class="row">
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Titulo</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_titulo" id="busqueda_titulo" placeholder="Titulo">
          </div>
        </div>
      </div>
       <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Autor</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_autor" id="busqueda_autor" placeholder="Autor">
          </div>
        </div>
      </div>
     <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Edición</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_edicon" id="busqueda_edicon" placeholder="Edición">
          </div>
        </div>
      </div>
    <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Editorial</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_editorial" id="busqueda_editorial" placeholder="Editorial">
          </div>
        </div>
      </div>  
  </div>
        <div class="opciones">Titulo<input type="checkbox" name="opcion[]" id="opcion" value="titulo" checked></div>
        <div class="opciones">Autor<input type="checkbox" name="opcion[]" id="opcion" value="autor" checked></div>
        <div class="opciones">Edicion<input type="checkbox" name="opcion[]" id="opcion" value="edicion" checked></div>
        <div class="opciones">Año<input type="checkbox" name="opcion[]" id="opcion" value="year" checked></div>
        <div class="opciones">Isbn<input type="checkbox" name="opcion[]" id="opcion" value="isbn" checked></div>
        <div class="opciones">Folio<input type="checkbox" name="opcion[]" id="opcion" value="folio" checked></div>
        <div class="opciones">c_cotizada<input type="checkbox" name="opcion[]" id="opcion" value="c_cotizada" checked></div>
        <div class="opciones">c_adquirida<input type="checkbox" name="opcion[]" id="opcion" value="c_adquirida" checked></div>
        <div class="opciones">c_recibida<input type="checkbox" name="opcion[]" id="opcion" value="c_recibida" checked></div>
        <div class="opciones">c_existente<input type="checkbox" name="opcion[]" id="opcion" value="c_existente" checked></div>
        <div class="opciones">Materia<input type="checkbox" name="opcion[]" id="opcion" value="materia" checked></div>
        <div class="opciones">Provedor<input type="checkbox" name="opcion[]" id="opcion" value="provedor" checked></div>
        <div class="opciones">Factura<input type="checkbox" name="opcion[]" id="opcion" value="factura" checked></div>
      <br><br>
  <div class="rectangular">
  <input id="btn_busqueda" type="submit" name="submit" target="iframelibro" value="Buscar " />
  </div> 
</form>
  
<article>
        <div class="embed-responsive embed-responsive-16by9">
        <iframe id="iframebusqueda"name="iframebusqueda" src="" frameborder="0">
         </iframe>
         </div>
</article>


</body>
</html>
<?php
}else{
  ?>
<script>
  alert('No has iniciado sesion');
  window.location="../index.html";
</script>
<?php
}
?>

