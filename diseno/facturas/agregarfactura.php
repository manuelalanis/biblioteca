<!DOCTYPE html>
<html lang="en">
<head>
  <title>Agregar factura</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <style>
  /*
  Max width before this PARTICULAR table gets nasty
  This query will take effect for any screen smaller than 760px
  and also iPads specifically.
  */
  @media
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr {
      display: block;
    }

    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr {
      position: absolute;
      top: -9999px;
      left: -9999px;
    }

    tr { border: 1px solid #ccc; }

    td {
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee;
      position: relative;
      padding-left: 50%;
    }

    td:before {
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
    }

    /*
    Label the data
    */
    td:nth-of-type(1):before { content: "First Name"; }
    td:nth-of-type(2):before { content: "Last Name"; }
    td:nth-of-type(3):before { content: "Job Title"; }
    td:nth-of-type(4):before { content: "Favorite Color"; }
    td:nth-of-type(5):before { content: "Wars of Trek?"; }
    td:nth-of-type(6):before { content: "Porn Name"; }
    td:nth-of-type(7):before { content: "Date of Birth"; }
    td:nth-of-type(8):before { content: "Dream Vacation City"; }
    td:nth-of-type(9):before { content: "GPA"; }
    td:nth-of-type(10):before { content: "Arbitrary Data"; }
  }

  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width: 320px; }
    }

  /* iPads (portrait and landscape) ----------- */
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }
  #iframelibro{
    width: 1200px;
    height: 1000px;
  }
  
  

button:hover {
  cursor: pointer;
  transition: background-color 0.5s, color 0.5s;
  
}

/*------MENU Buttons-----------*/

.menu_ #elemento {
  padding: 15px 64px;
  /*margin: 30px 25px 30px 25px;*/ 
  display: inline-block;
  font-family: segoe UI;
  font-size: 15px;
  outline: none;
  color: #fff;
  font-weight: bold;
}


/*------MENU Buttons-----------*/

/*------Rectangular Buttons-----------*/

.rectangular button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  font-weight: bold;
  position: relative;
  top: 20px;
  right: 100px;
}


/*-------Round Buttons------------*/

.round button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  border-radius: 40px;
}

/*-------Round Buttons 2------------*/
.round_2 button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  border-radius: 30px;
  font-weight: bold;
}
/*-----------button style--------*/

#boton_busqueda {
    background:#0e83cd;
  border: 2px solid #0e83cd;
}

#boton_busqueda:hover {
  background:#53a6d7;
  border-color: #0e83cd;
  color: white;
}

#boton_busqueda:active {
  position: relative;
  background: #fff;
  border-color: #fff;
}
#titulo{
  color: white;
}
  </style>
  <script>
  $(document).ready(function(){
    $("#materia_facultad").focus();
  });
  </script>
</head>
<body>


<div class="container-fluid">
  <div class="text-center">
    <h1></h1>
    <p class="lead">Agregar factura</p>
    <p>Nota: No olvide llenar todos los campos.</p>
  </div>
    <div class="row">
     <form method="POST" action="enviarfactura.php">
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Nombre Factura:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="factura_nombre" id="factura_nombre" placeholder="Provedor (Nombre)">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel4" class="col-md-4 control-label">Numero Factura:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="factura_numero" id="factura_numero" placeholder="Numero Factura">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel4" class="col-md-4 control-label">Monto total:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="factura_total" id="factura_total" placeholder="Monto total">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input5" class="col-md-4 control-label">Titulos facturados:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="factura_titulo" id="factura_titulo" placeholder="Nombre Factura ">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input7" class="col-md-4 control-label">Volumenes facturados:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="factura_volumenes" id="factura_volumenes" placeholder="Volumenes facturados">
          </div>
        </div>
      </div>
        <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input5" class="col-md-4 control-label">Campus:</label>
          <div class="col-md-8">
            <select class="form-control" name="factura_campus" id="factura_campus" onchange=''>
              <option value="Tijuana">Tijuana</option>
              <option value="Tecate">Tecate</option>
              <option value="Rosarito">Rosarito</option>
              <option value="Valle de las palmas">Valle de las palmas</option>
              <option value="Otro">Otro</option>
            </select>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <!-- <label for="input7" class="col-md-4 control-label">Titulos facturados:</label> -->
          <div class="col-md-8">
            <!-- <input type="text" class="form-control" name="materia_materia" id="materia_materia" placeholder="Titulos facturados"> -->
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
        </div>
      </div>      
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">

        </div>
        
        <div class="rectangular">
        <a >
          <button  type="submit" id="boton_busqueda"> <span class="glyphicon glyphicon-plus"></span>Agregar </button>
        </a>  
        </div> 
      
      </div>
    </div>
  </form>
  
</div><!-- /.container

</body>
</html>
