<!DOCTYPE html>
<html lang="en">
<head>
  <title>Libros</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script>
  // $(document).ready(function(){
  // 	$('#iframelibro').attr('src','libros/agregarlibro.php');
  //   $("#libros_titulo").focus();
  // });
  </script>
  <style>
  #iframebusqueda{
    position:absolute;
    background-color: white;
  }
  .opciones{
    margin: 5px;
    display: inline-block;
    font-size: 17px;
  }
  .opciones input[type='checkbox']{
    background-color: red;
    width: 15px;
    height: 15px;
  }
  .oculto{
    display: none;
  }


  </style>
  </head>
<body>

  <iframe class="oculto"id="iframelibro"name="iframelibro" src="" frameborder="0">
  </iframe>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand active" href="../inicio.html">Biblioteca Uabc</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../libros.php">Libros</a></li>
        <li><a href="../provedores.php">Provedores</a></li>
        <li><a href="../materias.php">Materias</a></li>
        <li><a href="../facturas.php">Facturas</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>


<div class="container">
  <h3>Libros</h3>
  <ul class="nav nav-pills">
    <li><a href="../libros.php" >Agregar</a></li>
    <li><a href="buscar2.php">Buscar y exportar</a></li>
    <li><a href="modificar2.php" >Modificar</a></li>
    <li><a href="seleccionarlibros.php">Eliminar</a></li>
  </ul>
</div>

  <p class="lead">Buscar por:</p>
<form action="select.php" method="post" target="iframebusqueda">
     <div class="row">
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Titulo</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_titulo" id="busqueda_titulo" placeholder="Titulo">
          </div>
        </div>
      </div>
       <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Autor</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_autor" id="busqueda_autor" placeholder="Autor">
          </div>
        </div>
      </div>
     <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Edición</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_edicon" id="busqueda_edicon" placeholder="Edición">
          </div>
        </div>
      </div>
    <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Editorial</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_editorial" id="busqueda_editorial" placeholder="Editorial">
          </div>
        </div>
      </div>  
  </div>
        <div class="opciones">Titulo<input type="checkbox" name="opcion[]" id="opcion" value="titulo"></div>
        <div class="opciones">Autor<input type="checkbox" name="opcion[]" id="opcion" value="autor"></div>
        <div class="opciones">Edicion<input type="checkbox" name="opcion[]" id="opcion" value="edicion"></div>
        <div class="opciones">Año<input type="checkbox" name="opcion[]" id="opcion" value="year"></div>
        <div class="opciones">Isbn<input type="checkbox" name="opcion[]" id="opcion" value="isbn"></div>
        <div class="opciones">Folio<input type="checkbox" name="opcion[]" id="opcion" value="folio"></div>
        <div class="opciones">c_cotizada<input type="checkbox" name="opcion[]" id="opcion" value="c_cotizada"></div>
        <div class="opciones">c_adquirida<input type="checkbox" name="opcion[]" id="opcion" value="c_adquirida"></div>
        <div class="opciones">c_recibida<input type="checkbox" name="opcion[]" id="opcion" value="c_recibida"></div>
        <div class="opciones">c_existente<input type="checkbox" name="opcion[]" id="opcion" value="c_existente"></div>
        <div class="opciones">materia<input type="checkbox" name="opcion[]" id="opcion" value="materia"></div>
        <div class="opciones">nombre_provedor<input type="checkbox" name="opcion[]" id="opcion" value="provedor"></div>
      <br><br>
  <div class="rectangular">
  <input id="btn_busqueda" type="submit" name="submit" target="iframelibro" value="Buscar " />
  </div> 
</form>
  
<article>
        <div class="embed-responsive embed-responsive-16by9">
        <iframe id="iframebusqueda"name="iframebusqueda" src="" frameborder="0">
         </iframe>
         </div>
</article>


<!-- <article id="articulo">
          <section >
              <div class="embed-responsive embed-responsive-16by9">
              <iframe  class="embed-responsive-item" id="iframeprovedor"name="iframeprovedor" src="" frameborder="0">
              </div>
            </iframe>
           </section>
    </article>
 -->

</body>
</html>
