<!DOCTYPE html>
<html lang="en">
<head>
  <title>Libros</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script>
  // $(document).ready(function(){
  // 	$('#iframelibro').attr('src','libros/agregarlibro.php');
  //   $("#libros_titulo").focus();
  // });
  </script>
  <style>
  #iframebusqueda{
    position:absolute;
    background-color: white;
  }
  .oculto{
    display: none;
  }
  </style>
  </head>
<body>
  <iframe class="oculto"id="iframelibro"name="iframelibro" src="" frameborder="0">
  </iframe>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand active" href="inicio.html">Biblioteca Uabc</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="libros.php">Libros</a></li>
        <li><a href="../provedores.php">Provedores</a></li>
        <li><a href="../materias.php">Materias</a></li>
        <li><a href="../facturas.php">Facturas</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>


<div class="container">
  <h3>Libros</h3>
  <ul class="nav nav-pills">
    <li><a href="../libros.php" >Agregar</a></li>
    <li><a href="buscar2.php">Buscar y exportar</a></li>
    <li><a href="modificar2.php" >Modificar</a></li>
    <li><a href="seleccionarlibros.php">Eliminar</a></li>
  </ul>
</div>


  <p class="lead">Eliminar por:</p>
<form action="motoreliminar.php" method="post" target="iframebusqueda">
     <div class="row">
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Titulo</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_titulo" id="busqueda_titulo" placeholder="Titulo">
          </div>
        </div>
      </div>
       <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Autor</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_autor" id="busqueda_autor" placeholder="Autor">
          </div>
        </div>
      </div>
     <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Edición</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_edicon" id="busqueda_edicon" placeholder="Edición">
          </div>
        </div>
      </div>
    <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Editorial</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="busqueda_editorial" id="busqueda_editorial" placeholder="Editorial">
          </div>
        </div>
      </div>  
  </div>
  <br><h6>Los campos:</h6>
  <input type="checkbox" value="">Titulo</input>
  <input type="checkbox" value="">Autor</input>
  <input type="checkbox" value="">Edición</input>
  <input type="checkbox" value="">Año</input>
  <input type="checkbox" value="">Isbn</input>
  <input type="checkbox" value="">Materia</input>
  <input type="checkbox" value="">Factura</input>
  <br><br>
  <div class="rectangular">
  <input id="btn_busqueda" type="submit" name="submit" target="iframelibro" value="Buscar " />
  </div> 
</form>
  
<article>
        <div class="embed-responsive embed-responsive-16by9">
        <iframe id="iframebusqueda"name="iframebusqueda" src="" frameborder="0">
         </iframe>
         </div>
</article>


<!-- <article id="articulo">
          <section >
              <div class="embed-responsive embed-responsive-16by9">
              <iframe  class="embed-responsive-item" id="iframeprovedor"name="iframeprovedor" src="" frameborder="0">
              </div>
            </iframe>
           </section>
    </article>
 -->

</body>
</html>
