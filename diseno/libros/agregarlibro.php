<!DOCTYPE html>
<html lang="en">
<head>
  <title>Agregar libro</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <style>
  /*
  Max width before this PARTICULAR table gets nasty
  This query will take effect for any screen smaller than 760px
  and also iPads specifically.
  */
  @media
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr {
      display: block;
    }

    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr {
      position: absolute;
      top: -9999px;
      left: -9999px;
    }

    tr { border: 1px solid #ccc; }

    td {
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee;
      position: relative;
      padding-left: 50%;
    }

    td:before {
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
    }

    /*
    Label the data
    */
    td:nth-of-type(1):before { content: "First Name"; }
    td:nth-of-type(2):before { content: "Last Name"; }
    td:nth-of-type(3):before { content: "Job Title"; }
    td:nth-of-type(4):before { content: "Favorite Color"; }
    td:nth-of-type(5):before { content: "Wars of Trek?"; }
    td:nth-of-type(6):before { content: "Porn Name"; }
    td:nth-of-type(7):before { content: "Date of Birth"; }
    td:nth-of-type(8):before { content: "Dream Vacation City"; }
    td:nth-of-type(9):before { content: "GPA"; }
    td:nth-of-type(10):before { content: "Arbitrary Data"; }
  }

  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width: 320px; }
    }

  /* iPads (portrait and landscape) ----------- */
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }
  #iframelibro{
    width: 1200px;
    height: 1000px;
  }
  
  

button:hover {
  cursor: pointer;
  transition: background-color 0.5s, color 0.5s;
  
}

/*------MENU Buttons-----------*/

.menu_ #elemento {
  padding: 15px 64px;
  /*margin: 30px 25px 30px 25px;*/ 
  display: inline-block;
  font-family: segoe UI;
  font-size: 15px;
  outline: none;
  color: #fff;
  font-weight: bold;
}


/*------MENU Buttons-----------*/

/*------Rectangular Buttons-----------*/

.rectangular button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  font-weight: bold;
  position: relative;
  top: 20px;
  right: 100px;
}


/*-------Round Buttons------------*/

.round button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  border-radius: 40px;
}

/*-------Round Buttons 2------------*/
.round_2 button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  border-radius: 30px;
  font-weight: bold;
}
/*-----------button style--------*/

#boton_busqueda {
    background:#0e83cd;
  border: 2px solid #0e83cd;
}

#boton_busqueda:hover {
  background:#53a6d7;
  border-color: #0e83cd;
  color: white;
}

#boton_busqueda:active {
  position: relative;
  background: #fff;
  border-color: #fff;
}
#titulo{
  color: white;
}
  </style>

 <script>

  function InsertarBD(){
     var tabla = document.getElementById("tblDinamica");
      var message = "?datos="+tabla.rows.length;
    var cont =0;
    var x = new Array(((tabla.rows.length)-6)*6);
      for (var i = 6; i < tabla.rows.length; i++){
                var row = tabla.rows[i];
                for (var j = 0; j < row.cells.length-1; j++){
                    var cell = row.cells[j];
          x[cont++]=cell.innerHTML;
          message += cell.innerHTML;
                }
            }
      window.location.assign("\\bTijuana\\formulario.php?total="+tabla.rows.length+"&message="+x);
  } 
  
  function agregarFila(){
        var titulo = document.getElementById("titulo").value;
        var autor = document.getElementById("autor").value;
        var editorial = document.getElementById("editorial").value;
        var edicion = document.getElementById("edicion").value;
    var isbn = document.getElementById("isbn").value;
    var cantidad = document.getElementById("cantidad").value;
    var tabla = document.getElementById("tblDinamica");
    tabla.innerHTML = tabla.innerHTML +
        "<tr bgcolor=\"#F8FBEF\" name=\"fila\"><span class=\"auto-style6\">"+
                  "<td  colspan=\"2\">"+(titulo==""?"&nbsp":titulo)+"</td>"+
          "<td>"+(autor==""?"&nbsp":autor)+"</td>"+
          "<td>"+(editorial==""?"&nbsp":editorial)+"</td>"+
          "<td>"+(edicion==""?"&nbsp":edicion)+"</td>"+
      "<td>"+(isbn==""?"&nbsp":isbn)+"</td>"+
      "<td>"+(cantidad==""?"&nbsp":cantidad)+"</td>"+
          "<td align=\"center\"><input type=\"checkbox\" "+
            "name=\"marca\"/"+"></td>"+
                "</span></tr>";
      
    }

    function eliminarFila(){
    var tabla =  document.getElementById("tblDinamica"); 
    var marcas = document.getElementsByName("marca");
    for( i = marcas.length - 1 ; i >= 0 ; --i ){
      if(marcas[i].checked){
        tabla.removeChild(marcas[i].parentNode.parentNode.parentNode);        
      }
    }      
     }

    </script>

</head>
<body>


<div class="container-fluid">
  <div class="text-center">
    <h1></h1>
    <p class="lead">Agregar libro
    </p>
    <p>Nota: No olvide llenar todos los campos.</p>
  </div>
    <div class="row">
     <form method="POST" action="enviarlibro.php">
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Titulo</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="libros_titulo" id="libros_titulo" placeholder="Titulo">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel4" class="col-md-4 control-label">ISBN:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="libros_isbn" id="libros_isbn" placeholder="ISBN">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input5" class="col-md-4 control-label">Autor:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="libros_autor" id="libros_autor" placeholder="Autor  ">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input6" class="col-md-4 control-label">Editorial:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="libros_editorial" id="libros_editorial" placeholder="Editorial">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input7" class="col-md-4 control-label">Edicion:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="libros_edicion" id="libros_edicion" placeholder="Edicion">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input8" class="col-md-4 control-label">Materia:</label>
          <div class="col-md-8">
            <h6 id="agre_2"></h6>
              <select class="form-control"id="agre_2"name="idmarca" id="" onchange=''>
                <?php 
                require("../conectar.php"); 
                $idmarca = $_POST["idmarca"];
                if(isset($_POST["idmarca"]))
                  {
                    if ($idmarca >= 0)
                    {
                      
                    }
                    if ($idmarca == "Seleccione")
                    {
                      echo "<option >Seleccione</option>";
                    }
                  }
                  else
                  {
                    echo "<option >Seleccione</option>";
                  }
                ?>
                <?php
                  $query = "SELECT id_materia,materia,facultad,carrera,plan FROM `materia` ORDER BY `id_materia` ASC";
                  $checkuser = mysql_query($query,$conectado);
                  while ($row=mysql_fetch_array($checkuser))
                  {
                  if ($_POST['idmarca']==$row['id_materia']) 
                  {
                    echo "<option value='".$row['id_materia']."' ";
                     echo "SELECTED";
                    echo ">";
                    echo $row['materia'];
                    echo "</option>"; 
                    echo $_POST['idmarca'];   
                   }
                   else
                   {
                    echo "<option value='".$row['id_materia']."' ";
                    echo ">";
                    echo $row['facultad']." / ".$row['carrera']." / ".$row['materia']." / ".$row['plan'];
                    echo "</option>";
                     echo $_POST['idmarca'];    
                   }
                  }
                 ?>
              </select>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input9" class="col-md-4 control-label">Año:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="libros_year" id="libros_year" placeholder="Año">
          </div>
          <br>

          
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input10" class="col-md-4 control-label">Provedor:</label>
          <div class="col-md-8">
                <!-- <h5>Provedor:</h4> -->
                <select class="form-control" name="provedor" id="" onchange=''>
                <?php  
                $id = $_POST["provedor"];
                if(isset($_POST["idmarca"]))
                  {
                    if ($id >= 0)
                    {
                      
                    }
                    if ($id == "Seleccione")
                    {
                      echo "<option >Seleccione</option>";
                    }
                  }
                  else
                  {
                    echo "<option >Seleccione</option>";
                  }

                ?>
                <?php
                  $query = "SELECT id_provedor,editorial FROM `provedor` ORDER BY `id_provedor` ASC";
                  $checkuser = mysql_query($query,$conectado);
                  while ($row=mysql_fetch_array($checkuser))
                  {
                  if ($_POST['provedor']==$row['id_provedor']) 
                  {
                    echo "<option value='".$row['id_provedor']."' ";
                     echo "SELECTED";
                    echo ">";
                    echo $row['editorial'];
                    echo "</option>"; 
                    echo $_POST['provedor'];    
                   }
                   else
                   {
                    echo "<option value='".$row['id_provedor']."' ";
                    echo ">";
                    echo $row['editorial'];
                    echo "</option>";
                     echo $_POST['provedor'];
                   }
                  }
                 ?>
              </select>
          </div>
          <label for="input9" class="col-md-4 control-label">Folio:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="libros_folio" id="libros_folio" placeholder="Folio">
          </div>
        </div>
        <div class="rectangular">
        <a >
          <button  type="submit" id="boton_busqueda"> <span class="glyphicon glyphicon-plus"></span>Agregar </button>
        </a>  
        </div> 
      
      </div>
    </div>
  </form>
  
</div><!-- /.container

</body>
</html>
