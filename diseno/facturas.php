<!DOCTYPE html>
<html lang="en">
<head>
  <title>Facturas</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script>
  $(document).ready(function(){
    $('#iframelibro').attr('src','facturas/agregarfactura.php');
    $("#libros_titulo").focus();
  });
  </script>
  <style>
  #iframelibro{
    width: 1100px;
    height: 1000px;
    left: 70px;
  }
  </style>
  </head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand active" href="inicio.html">Biblioteca Uabc</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li ><a href="libros.php">Libros</a></li>
        <li ><a href="provedores.php">Provedores</a></li>
        <li ><a href="materias.php">Materias</a></li>
        <li class="active"><a href="facturas.php">Facturas</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="container">
  <h3>Libros</h3>
  <ul class="nav nav-pills">
    <li><a href="facturas/agregarfactura.php"    target="iframelibro">Agregar</a></li>
    <li><a href="facturas/listarfacturas.php"    target="iframelibro">Listar y Modificar</a></li>
    <li><a href="facturas/seleccionarfacturas.php"target="iframelibro">Eliminar</a></li>
  </ul>
</div>
    <article id="articulo">
          <section >
              <div class="embed-responsive embed-responsive-16by9">
              <iframe  class="embed-responsive-item" id="iframelibro"name="iframelibro" src="" frameborder="0">
              </div>
            </iframe>
           </section>
    </article>


</body>
</html>
