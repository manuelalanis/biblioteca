<!DOCTYPE html>
<html lang="en">
<head>
  <title>Agregar provedor</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <style>
input{
    text-transform: uppercase;
  }
  #iframelibro{
    width: 1200px;
    height: 2000px;
  }
button:hover {
  cursor: pointer;
  transition: background-color 0.5s, color 0.5s;
  
}
/*------MENU Buttons-----------*/

.menu_ #elemento {
  padding: 15px 64px;
  /*margin: 30px 25px 30px 25px;*/ 
  display: inline-block;
  font-family: segoe UI;
  font-size: 15px;
  outline: none;
  color: #fff;
  font-weight: bold;
}
/*------MENU Buttons-----------*/

/*------Rectangular Buttons-----------*/

.rectangular button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 13px;
  outline: none;
  color: #fff;
  font-weight: bold;
  position: relative;
}
/*-------Round Buttons------------*/
.round button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  border-radius: 40px;
}

/*-------Round Buttons 2------------*/
.round_2 button {
  padding: 15px 64px;
  margin: 30px 25px 30px 25px;
  display: inline-block;
  font-family: segoe UI;
  font-size: 14px;
  outline: none;
  color: #fff;
  border-radius: 30px;
  font-weight: bold;
}
/*-----------button style--------*/

  #boton_busqueda {
  background:#0e83cd;
  border: 2px solid #0e83cd;
  }

  #boton_busqueda:hover {
  background:#53a6d7;
  border-color: #0e83cd;
  color: white;
  }
  #boton_busqueda:active 
  {
  position: relative;
  background: #fff;
  border-color: #fff;
  }
  #titulo
  {
  color: white;
  }
  </style>
  <script>
   function justNumbers(e)
        {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
        return true;
         
        return /\d/.test(String.fromCharCode(keynum));
        }

  $(document).ready(function(){
    $("#prov_nombre").focus();
  });

  function validateForm() {
    var a = document.forms["form_provedor"]["prov_nombre"].value;
    var b = document.forms["form_provedor"]["prov_direccion"].value;
    var c = document.forms["form_provedor"]["prov_telefono"].value;
    var d = document.forms["form_provedor"]["prov_editorial"].value;
    var e = document.forms["form_provedor"]["prov_responsable"].value;
    var f = document.forms["form_provedor"]["prov_director"].value;
    var g = document.forms["form_provedor"]["prov_pwd"].value;
    var h = document.forms["form_provedor"]["prov_usuario"].value;
    if (a&&b&&c&&d&&e&&f&&g&&h) {
        
    }else{
      alert('Hay algun campo vacio.')
        return false;
    }
    
    }

  </script>
</head>
<body>
</head>
<body>

<div class="container-fluid">
  <div class="text-center">
    <h1></h1>
    <p class="lead">Agregar provedor
    </p>
    <p>Nota: No olvide llenar todos los campos.</p>
  </div>
    <div class="row">
     <form name="form_provedor"method="POST" action="envioprovedor.php" onsubmit="return validateForm()">
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Provedor(Nombre*):</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="prov_nombre" id="prov_nombre" placeholder="Provedor(Nombre*)">
          </div>
        </div>
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Editorial(Nombre*):</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="prov_editorial" id="prov_editorial" placeholder="Editorial(Nombre*)">
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4">
        
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Direccion:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="prov_direccion" id="prov_direccion" placeholder="Direccion">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Telefono:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="prov_telefono" id="prov_telefono" placeholder="Telefono">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Responsable:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="prov_responsable" id="prov_responsable" placeholder="Responsable">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Director:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="prov_director" id="prov_director" placeholder="Director">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="" class="col-md-4 control-label"></label>
          <div class="col-md-8">
            <h6 id="agre_2"></h6>

          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Usuario:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="prov_usuario" id="prov_usuario" placeholder="Usuario">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="" class="col-md-4 control-label">Contraseña:</label>
          <div class="col-md-8">
          <input type="password" class="form-control" name="prov_pwd" id="prov_pwd" placeholder="Contraseña">
          </div>
        </div>
      </div>  
        <div class="col-sm-6 col-lg-6">
          <div class="rectangular">
        <a >
          <button  type="submit" id="boton_busqueda"> <span class="glyphicon glyphicon-plus"></span>Agregar </button>
        </a>  
        </div>
      </div>  
      </div>
    </div>
  </form>
  
</div>

</body>
</html>
