
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset='UTF-8'>

	<title>Modificaciónes libros</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="tabla.css">

	<!--[if !IE]><!-->
	<style>

	/*
	Max width before this PARTICULAR table gets nasty
	This query will take effect for any screen smaller than 760px
	and also iPads specifically.
	*/
	@media
	only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {

		/* Force table to not be like tables anymore */
		table, thead, tbody, th, td, tr {
			display: block;
		}

		/* Hide table headers (but not display: none;, for accessibility) */
		thead tr {
			position: absolute;
			top: -9999px;
			left: -9999px;
		}

		tr { border: 1px solid #ccc; }

		td {
			/* Behave  like a "row" */
			border: none;
			border-bottom: 1px solid #eee;
			position: relative;
			padding-left: 50%;
		}

		td:before {
			/* Now like a table header */
			position: absolute;
			/* Top/left values mimic padding */
			top: 6px;
			left: 6px;
			width: 45%;
			padding-right: 10px;
			white-space: nowrap;
		}

		/*
		Label the data
		*/
		td:nth-of-type(1):before { content: "Ejemplo"; }
		td:nth-of-type(2):before { content: "Ejemplo"; }
		td:nth-of-type(3):before { content: "Ejemplo"; }
		td:nth-of-type(4):before { content: "Ejemplo"; }
		td:nth-of-type(5):before { content: "Ejemplo"; }
		td:nth-of-type(6):before { content: "Ejemplo"; }
		td:nth-of-type(7):before { content: "Ejemplo"; }
		td:nth-of-type(8):before { content: "Ejemplo"; }
		td:nth-of-type(9):before { content: "Ejemplo"; }
		td:nth-of-type(10):before { content:"Ejemplo"; }
	}

	/* Smartphones (portrait and landscape) ----------- */
	@media only screen
	and (min-device-width : 320px)
	and (max-device-width : 480px) {
		body {
			padding: 0;
			margin: 0;
			width: 320px; }
		}

	/* iPads (portrait and landscape) ----------- */
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		body {
			width: 495px;
		}
	}

	</style>
	<!--<![endif]-->

</head>

<body>

<div id="demo-top-bar">
  <div id="demo-bar-inside">
    <h2 id="demo-bar-badge"></h2>
    <div id="demo-bar-buttons">
      <a class='header-button' href='/responsive-data-tables/'></a></div>
  	</div>

</div>
	<h2>Busqued:</h2>
	<br>
	<div id="">
	<table>
		<thead>
		<tr>
			<th>Titulo</th>
			<th>Autor</th>
			<th>Edicion</th>
			<th>Editorial</th>
			<th>Provedor</th>
			<th>Materia</th>
			<th>Factura</th>
			<th>Ejemplo</th>
			<th>Ejemplo</th>
			<th>Ejemplo </th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
		</tr>
		<tr>
		  	<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
		</tr>
		<tr>
		  <td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
		</tr>
		<tr>
		  <td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>		
		</tr>
		<tr>
		  <td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
			<td>Ejemplo</td>
		</tr>
		</tbody>
	</table>

	</div>

 <style type="text/css" style="display: none !important;">
	* {
		margin: 0;
		padding: 0;
	}
	body {
		overflow-x: hidden;
	}
	#demo-top-bar {
		display: none;
		text-align: left;
		background: #222;
		position: relative;
		zoom: 1;
		width: 100% !important;
		z-index: 6000;
		padding: 20px 0 20px;
	}
	#demo-bar-inside {
		width: 960px;
		margin: 0 auto;
		position: relative;
		overflow: hidden;
	}
	#demo-bar-buttons {
		padding-top: 10px;
		float: right;
	}
	#demo-bar-buttons a {
		font-size: 12px;
		margin-left: 20px;
		color: white;
		margin: 2px 0;
		text-decoration: none;
		font: 14px "Lucida Grande", Sans-Serif !important;
	}
	#demo-bar-buttons a:hover,
	#demo-bar-buttons a:focus {
		text-decoration: underline;
	}
	#demo-bar-badge {
		display: inline-block;
		width: 302px;
		padding: 0 !important;
		margin: 0 !important;
		background-color: transparent !important;
	}
	#demo-bar-badge a {
		display: block;
		width: 100%;
		height: 38px;
		border-radius: 0;
		bottom: auto;
		margin: 0;
		background: url(/images/examples-logo.png) no-repeat;
		background-size: 100%;
		overflow: hidden;
		text-indent: -9999px;
	}
	#demo-bar-badge:before, #demo-bar-badge:after {
		display: none !important;
	}
</style>
</body>

</html>