==========
Biblioteca UABC
==========

## Links:

+ [Test: Biblioteca](http://biblioteca.byronwd.com//)

## Inicio rapido

Generar la base de datos con: 

- [Descargar en fichero del repositorio](https://github.com/ManuelAlanis/biblioteca/archive/master.zip).: Download ZIP
- Generar el script.sql: recursos/generarbase.sql
- Tambien se puede generar desde el modelo recursos/modelobiblioteca.mwb

Ejecutar`mysql> source direccion/recursos/generarbase.sql` o aplicar forward engineering `mysql`  al modelo

**Manuel Alanis**

+ alanism@uabc.edu.mx
