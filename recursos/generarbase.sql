-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Eu
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Eu7
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Eu7
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Eu7` DEFAULT CHARACTER SET utf8 ;
USE `Eu7` ;

-- -----------------------------------------------------
-- Table `Eu7`.`compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Eu7`.`compras` (
  `no_id` INT(11) NOT NULL AUTO_INCREMENT,
  `factura_nombre` VARCHAR(45) NOT NULL,
  `factura_numero` VARCHAR(45) NULL,
  `precio` VARCHAR(45) NULL DEFAULT NULL,
  `titulos_facturados` VARCHAR(45) NULL,
  `volumenes` VARCHAR(45) NULL,
  `campus` VARCHAR(45) NULL,
  PRIMARY KEY (`no_id`),
  UNIQUE INDEX `no_id_UNIQUE` (`no_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Eu7`.`materia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Eu7`.`materia` (
  `id_materia` INT(11) NOT NULL AUTO_INCREMENT,
  `materia` VARCHAR(45) NULL DEFAULT NULL,
  `plan` VARCHAR(45) NULL DEFAULT NULL,
  `carrera` VARCHAR(45) NULL DEFAULT NULL,
  `facultad` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_materia`),
  UNIQUE INDEX `id_materia_UNIQUE` (`id_materia` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Eu7`.`provedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Eu7`.`provedor` (
  `id_provedor` INT(11) NOT NULL AUTO_INCREMENT,
  `editorial_provedor` VARCHAR(45) NULL DEFAULT NULL,
  `nombre_provedor` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NULL DEFAULT NULL,
  `responsable` VARCHAR(45) NULL DEFAULT NULL,
  `director` VARCHAR(45) NULL DEFAULT NULL,
  `usuario_provedor` VARCHAR(45) NOT NULL,
  `pwd_provedor` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_provedor`),
  UNIQUE INDEX `id_provedor_UNIQUE` (`id_provedor` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Eu7`.`libros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Eu7`.`libros` (
  `id_libro` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(100) NULL DEFAULT NULL,
  `autor` VARCHAR(60) NULL DEFAULT NULL,
  `edicion` VARCHAR(60) NULL DEFAULT NULL,
  `year` VARCHAR(60) NULL DEFAULT NULL,
  `isbn` VARCHAR(60) NULL DEFAULT NULL,
  `materia_id_materia` INT(11) NOT NULL,
  `compras_no_id` INT(11) NULL,
  `provedor_id_provedor` INT(11) NOT NULL,
  `folio` VARCHAR(70) NULL,
  `editorial_libros` VARCHAR(70) NULL,
  `c_cotizada` VARCHAR(70) NULL,
  `c_adquirida` VARCHAR(45) NULL,
  `c_recibida` VARCHAR(45) NULL,
  `c_existente` VARCHAR(45) NULL,
  `costo` VARCHAR(45) NULL,
  PRIMARY KEY (`id_libro`),
  UNIQUE INDEX `id_libro_UNIQUE` (`id_libro` ASC),
  INDEX `fk_libros_materia1_idx` (`materia_id_materia` ASC),
  INDEX `fk_libros_compras1_idx` (`compras_no_id` ASC),
  INDEX `fk_libros_provedor1_idx` (`provedor_id_provedor` ASC),
  CONSTRAINT `fk_libros_compras1`
    FOREIGN KEY (`compras_no_id`)
    REFERENCES `Eu7`.`compras` (`no_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libros_materia1`
    FOREIGN KEY (`materia_id_materia`)
    REFERENCES `Eu7`.`materia` (`id_materia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libros_provedor1`
    FOREIGN KEY (`provedor_id_provedor`)
    REFERENCES `Eu7`.`provedor` (`id_provedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

INSERT INTO `provedor` (`usuario_provedor`,`pwd_provedor`) values ('admin','admin');
